# Learning Rust

Project for learning the Rust programming language.

## Installing stuff

I'm following this [series of posts][1] to install the compiler and package manager using rustup.

[1]: https://medium.com/@pedrojordao/rust-from-the-ground-up-part-1-91b77f49f29c